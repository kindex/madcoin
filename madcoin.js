pragma solidity ^0.4.2;

contract owned {
	address public owner;

	function owned() {
		owner = msg.sender;
	}

	function changeOwner(address newOwner) onlyOwner {
		owner = newOwner;
	}

	modifier onlyOwner {
		require(msg.sender == owner);
		_;
	}
}

contract MadCoin is owned
{
    struct backer {
        bool exists;
        address addr;
        uint tokens;
        uint withdrawedTokens;
        uint earnedWei;
        uint lockedTillDate;
    }
    
    bool debug = true;
     
    mapping (address => backer) public backers;
    address[] public backerByIndex;
    
    uint _totalSupply;
    uint tokenPrice = 100000000000000000;
    uint decimal = 1000;
    bool public buyTokenEnabled = false;

    uint public crowdsaleGoal = 10;
    uint public crowdsaleStartTime;
    uint public crowdsaleEndTime;
    uint public crowdsaleEarnedWei;
    uint public crowdsaleMinWei = 50 finney;
    uint public crowdsaleTokenLockDate;

    event tokenBoughtEvent(address _address, uint amount, uint tokenCountDecimal);
    event transferEvent(address indexed from, address indexed to, uint tokenCountDecimal);
    event mintEvent(address indexed to, uint tokenCountDecimal);
    event buyoutEvent(address indexed to, uint _wei, uint tokens);
    event buyoutErrorEvent(address indexed to, uint _wei, uint tokens);
    event debugEvent(uint totalTokens, uint ownedTokenPercent, uint backerBuyoutTokens, uint valueToSend);
	event changeEvent(address buyer, uint incomingMoney, uint tokenCount, uint change);
	event withdrawEvent(address sender, uint amount, uint left);
	event addEthToContractEvent(address sender, uint amount, uint total);
	event setupCrowdsaleEvent(uint crowdsaleStartTime, uint crowdsaleEndTime, uint crowdsaleGoal);

    modifier afterDeadline() { if (now >= crowdsaleEndTime) _; }
				    
    function MadCoin() payable
    {
        owner = msg.sender;

        if (debug)
        {
	        setupCrowdsale(10, 15, 0);
	        startCrowdsale();
        }
    }

    function setupCrowdsale(uint newCrowdsaleGoalEth, uint crowdsaleDurationInMinutes, uint tokenLockDate) onlyOwner
    {
	    crowdsaleGoal = newCrowdsaleGoalEth * 1 ether;
	    crowdsaleEndTime = crowdsaleStartTime + crowdsaleDurationInMinutes*60;
	    setupCrowdsaleEvent(crowdsaleStartTime, crowdsaleEndTime, crowdsaleGoal);
	    crowdsaleTokenLockDate = tokenLockDate;
	}

    function startCrowdsale() onlyOwner
    {
	    crowdsaleStartTime = now;
	    buyTokenEnabled = true;
	    crowdsaleEarnedWei = 0;
	}

    function allowedToReceiveMoneyFromBackers() constant returns (bool result)
    {
        result = buyTokenEnabled && now < crowdsaleEndTime && (crowdsaleEarnedWei + crowdsaleMinWei <= crowdsaleGoal);
    }

    function crowdsaleProgressEth() constant returns (uint result)
    {
        result = decimal * crowdsaleEarnedWei / crowdsaleGoal;
    }

    function crowdsaleProgressTime() constant returns (uint result)
    {
        result = decimal * (now - crowdsaleStartTime) / (crowdsaleEndTime - crowdsaleStartTime);
    }

    function crowdsaleLeftTime() constant returns (int result)
    {
        result = int(crowdsaleEndTime) - int(now);
    }
    
    function buyToken() internal
    {
        require(msg.value > crowdsaleMinWei);
        require(allowedToReceiveMoneyFromBackers());
		require(msg.sender != owner);

        if (!debug)
        {
			require(msg.value >= crowdsaleMinWei);
		}

		uint value = msg.value;
		uint change = 0;
		if (crowdsaleEarnedWei + msg.value > crowdsaleGoal)
		{
			value = crowdsaleGoal - crowdsaleEarnedWei;
			change = msg.value - value;
		}

        uint tokenCount = value * decimal / tokenPrice;
        
        tokenBoughtEvent(
            msg.sender,
            msg.value,
            tokenCount
        );
		_totalSupply += value;
		crowdsaleEarnedWei += value;
		createBacker(msg.sender);

		backers[msg.sender].tokens += tokenCount;

		if (backers[msg.sender].lockedTillDate == 0)
		{
			backers[msg.sender].lockedTillDate = crowdsaleTokenLockDate;
		}

		if (change > 0)
		{
			if (msg.sender.send(change))
			{
				changeEvent(msg.sender, msg.value, tokenCount, change);
			}
		}
    }

   /* The function without name is the default function that is called whenever anyone sends funds to a contract */
    function () payable 
    {
	    buyToken();
    }

    function createBacker(address addr) internal
    {
        if (!backers[addr].exists)
        {
            backers[addr].exists = true;
            backers[addr].addr = addr;
            backerByIndex.push(addr);
        }
    }
    
    function allowBuyToken() onlyOwner
    {
        buyTokenEnabled = true;
    }

    function disallowBuyToken() onlyOwner
    {
        buyTokenEnabled = false;
    }

    function setTokenPrice(uint newTokenPriceWei) onlyOwner
    {
        tokenPrice = newTokenPriceWei;
    }

    function getTokenPrice() constant returns (uint result)
    {
        result = tokenPrice;
    }
    
    function mintTokenDecimal(address target, uint tokenCountDecimal, uint lockedTillDate) onlyOwner
    {
		createBacker(target);
		backers[target].tokens += tokenCountDecimal;
		if (backers[target].lockedTillDate == 0) // leave old lock Date if was set
		{
			backers[target].lockedTillDate = lockedTillDate;
		}

		mintEvent(target, tokenCountDecimal);
    }

    function mintToken(address target, uint tokenCount, uint lockedTillDate) onlyOwner
    {
	    mintTokenDecimal(target, tokenCount * decimal, lockedTillDate);
    }
	
/*    function myTokenCount() constant returns (uint result)
    {
        result = backers[msg.sender].tokens;
    }
*/
    function myWithdrawedTokenCount() constant returns (uint result)
    {
        result = backers[msg.sender].withdrawedTokens;
    }

    function myEarnedWei() constant returns (uint result)
    {
        result = backers[msg.sender].earnedWei;
    }

    function totalCoinsInTheWorld() constant returns (uint result)
    {
        result = 0;
        for (uint256 i = 0; i < backerByIndex.length; i++)
        {
            result += backers[backerByIndex[i]].tokens;
        }
    }

    function totalearnedWeiInTheWorld() constant returns (uint result)
    {
        result = 0;
        for (uint256 i = 0; i < backerByIndex.length; i++)
        {
            result += backers[backerByIndex[i]].earnedWei;
        }
    }

    function buyout() payable onlyOwner afterDeadline
    {
        uint leftTokens = totalCoinsInTheWorld();
        require(leftTokens > 0);
        
		uint value = msg.value;
		uint tokensToBuyout = decimal*value/tokenPrice; // as decimal
		bool all = false;

		if (tokensToBuyout >= leftTokens)
		{
			all = true;
			tokensToBuyout = leftTokens;
			value = tokensToBuyout * tokenPrice / decimal;
			uint change = msg.value - value;
	
			if (msg.sender.send(change))
			{
				changeEvent(msg.sender, msg.value, tokensToBuyout, change);
			}
		}

        for (uint256 k = 0; k < backerByIndex.length; k++)
        {
            address backerAddr = backerByIndex[k];
            uint ownedTokenPercent = (backers[backerAddr].tokens * decimal)/leftTokens; // as decimal

            uint tokensToBuyoutFromBacker;
            if (all)
            {
            	tokensToBuyoutFromBacker = backers[backerAddr].tokens;
            }
            else
            {	
            	tokensToBuyoutFromBacker = ownedTokenPercent*tokensToBuyout / decimal;
            }

            if (backers[backerAddr].tokens < tokensToBuyoutFromBacker)
            {
   	        	tokensToBuyoutFromBacker = backers[backerAddr].tokens;
   	        }
            
            uint ethToSend = value*ownedTokenPercent / decimal;

            if (debug)
            {
	            debugEvent(tokensToBuyout, ownedTokenPercent, tokensToBuyoutFromBacker, ethToSend);
            }

            if (ethToSend > 0 && backerAddr.send(ethToSend))
            {
	            backers[backerAddr].tokens -= tokensToBuyoutFromBacker;
	            backers[backerAddr].withdrawedTokens += tokensToBuyoutFromBacker;
	            backers[backerAddr].earnedWei += ethToSend;

                buyoutEvent(backerAddr, ethToSend, tokensToBuyoutFromBacker);
            }
            else
            {
                buyoutErrorEvent(backerAddr, ethToSend, tokensToBuyoutFromBacker);
            }
        }
    }

    function killContract () onlyOwner
    {
        if (debug)
        {
            selfdestruct(owner);
        }
    }

    function balanceOf(address backerAddr) constant returns (uint256 balance) 
    {
		return backers[backerAddr].tokens;
    }

    function transfer(address _to, uint256 _value)
    {
        require(_to != 0x0);
        require(balanceOf(msg.sender) >= _value);
        require(backers[msg.sender].lockedTillDate < now);

        backers[msg.sender].tokens -= _value;
        backers[_to].tokens += _value;
        transferEvent(msg.sender, _to, _value);
    }

	function withdraw(uint amount) onlyOwner afterDeadline
	{
		require(this.balance >= amount);

		if (owner.send(amount)) 
		{
			withdrawEvent(msg.sender, amount, this.balance - amount);
		}
	}

    function addEthToContract() payable onlyOwner
    {
		addEthToContractEvent(msg.sender, msg.value, this.balance);
    }
}
